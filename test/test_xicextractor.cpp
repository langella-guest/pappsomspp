
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#include <iostream>
#include <odsstream/odsdocreader.h>
#include <odsstream/odsdocwriter.h>

#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/xicextractor/msrunxicextractorfactory.h>
#include <QDebug>
#include <QtCore>
#include <QFile>
#include "config.h"
//#include "common.h"

// make test ARGS="-V -I 18,18"

using namespace std;


int
main(int argc, char *argv[])
{
  // QCoreApplication a(argc, argv);

  qDebug() << "init test XIC extractor";
  cout << endl << "..:: Test XIC ::.." << endl;
  QTime timer;

#if USEPAPPSOTREE == 1
  pappso::MsFileAccessor file_access(
    //"/gorgone/pappso/data_extraction_pappso/mzXML/"
    "/espace/bio/mzxml/"
    "20120906_balliau_extract_1_A01_urnb-1.mzXML",
    "file");
  pappso::MsRunReaderSPtr msrun =
    file_access.getMsRunReaderSPtrByRunId("", "runa1");

  pappso::MsRunXicExtractorFactory::getInstance().setTmpDir("/tmp");

  pappso::MzRange mass_range(600, pappso::PrecisionFactory::getPpmInstance(10));
  // MassRange mass_range_b(600.2, Precision::getPpmInstance(10));

  pappso::MsRunXicExtractorInterfaceSp extractor_pwiz =
    pappso::MsRunXicExtractorFactory::getInstance().buildMsRunXicExtractorSp(
      msrun);

  extractor_pwiz->setXicExtractMethod(pappso::XicExtractMethod::max);

  pappso::XicCstSPtr xic_pwiz = extractor_pwiz.get()->getXicCstSPtr(mass_range);

  cout << endl << "..:: XIC extractor pwiz is OK ::.." << endl;

  cout << endl << "building XicExtractorDiskSp .." << endl;
  timer.start();
  pappso::MsRunXicExtractorInterfaceSp extractor_disk =
    pappso::MsRunXicExtractorFactory::getInstance()
      .buildMsRunXicExtractorDiskSp(msrun);
  cout << endl
       << "XicExtractorDiskSp built " << timer.elapsed() / 1000 << " seconds"
       << endl;

  pappso::XicCstSPtr xic_disk = extractor_disk.get()->getXicCstSPtr(mass_range);

  if(*xic_pwiz != *xic_disk)
    {
      cerr << "*xic_pwiz != *xic_disk ERROR" << endl;

      for(std::size_t i = 0; i < xic_pwiz->size(); i++)
        {
          cerr << "i=" << i << " x=" << (*xic_pwiz)[i].x
               << " y=" << (*xic_pwiz)[i].y << " x=" << (*xic_disk)[i].x
               << " y=" << (*xic_disk)[i].y << endl;
        }
      return 1;
    }

  cout << endl << "..:: XIC extractor on disk is OK ::.." << endl;
  // return 0;

  cout << endl << "building XicExtractorDiskBufferSp .." << endl;
  timer.restart();
  pappso::MsRunXicExtractorInterfaceSp extractor_buffer =
    pappso::MsRunXicExtractorFactory::getInstance()
      .buildMsRunXicExtractorDiskBufferSp(msrun);
  cout << endl
       << "XicExtractorDiskBufferSp built " << timer.elapsed() / 1000
       << " seconds" << endl;

  pappso::XicCstSPtr xic_buffer =
    extractor_buffer.get()->getXicCstSPtr(mass_range);

  if(*xic_pwiz != *xic_buffer)
    {
      cerr << "*xic_pwiz != *xic_buffer ERROR" << endl;
      /*
      for(std::size_t i = 0; i < xic_pwiz->size(); i++)
        {
          cerr << "i=" << i << " " << (*xic_pwiz)[i].y << " "
               << (*xic_buffer)[i].y << endl;
        }
        */
      return 1;
    }

  cout << endl << "..:: XIC extractor buffered on disk is OK ::.." << endl;
#endif
  return 0;
}
