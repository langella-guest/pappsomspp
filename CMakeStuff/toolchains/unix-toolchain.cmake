message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)


find_package(ZLIB REQUIRED)


if(MAKE_TEST)
	find_package(QuaZip5 QUIET)

	if(NOT QuaZip5_FOUND)
		message(STATUS "QuaZip5 not yet found. Searching for it.")
		set(QuaZip_DIR ${CMAKE_MODULE_PATH})
		find_package(QuaZip REQUIRED)

		set(Quazip5_FOUND 1)
		set(Quazip5_INCLUDE_DIR ${QuaZip_INCLUDE_DIR})
		set(Quazip5_LIBRARY ${QuaZip_LIBRARY}) 
		if(NOT TARGET Quazip5::Quazip5)
			add_library(Quazip5::Quazip5 UNKNOWN IMPORTED)
			set_target_properties(Quazip5::Quazip5 PROPERTIES
				IMPORTED_LOCATION             "${Quazip5_LIBRARY}"
				INTERFACE_INCLUDE_DIRECTORIES "${Quazip5_INCLUDE_DIR}")
		endif()

	endif()

endif()


find_package(QCustomPlot REQUIRED)
# Per instructions of the lib author:
# https://www.qcustomplot.com/index.php/tutorials/settingup
message(STATUS "Setting definition -DQCUSTOMPLOT_USE_LIBRARY.")
if(NOT TARGET QCustomPlot::QCustomPlot)
	add_library(QCustomPlot::QCustomPlot UNKNOWN IMPORTED)
	set_target_properties(QCustomPlot::QCustomPlot PROPERTIES
		IMPORTED_LOCATION             "${QCustomPlot_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${QCustomPlot_INCLUDE_DIR}"
		INTERFACE_COMPILE_DEFINITIONS QCUSTOMPLOT_USE_LIBRARY)
endif()


add_definitions(-fPIC)

