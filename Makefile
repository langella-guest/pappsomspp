mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

all: 
	cd ../build-area && make 

.PHONY: clean
clean:
	cd ../build-area && make clean


