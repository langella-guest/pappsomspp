# CMake script for PAPPSOms++ library
# Authors: Olivier Langella & Filippo Rusconi
# Created: 03/03/2015
# Rework: April 2020 (Coronavirus confinement)

############################################################
############################################################
# Basic information about project

set(PROJECT pappsomspp)
project(${PROJECT} CXX)

set(PAPPSOMSPP_VERSION_MAJOR "0")
set(PAPPSOMSPP_VERSION_MINOR "7")
set(PAPPSOMSPP_VERSION_PATCH "4")
set(PAPPSOMSPP_VERSION "${PAPPSOMSPP_VERSION_MAJOR}.${PAPPSOMSPP_VERSION_MINOR}.${PAPPSOMSPP_VERSION_PATCH}")
# This VERSION is parseable by the makeOrigTarball script.
set(VERSION 0.7.4)

# Command to enable debug and tests
# cmake -DCMAKE_BUILD_TYPE=Debug  -DMAKE_TEST=1 -DUSEPAPPSOTREE=0 ..

#############################################################
#############################################################
# CMake configuration
cmake_minimum_required(VERSION 3.0)

set(HOME_DEVEL_DIR $ENV{HOME}/devel)
message("\n${BoldRed}The devel directory where all the development projects
should reside: ${HOME_DEVEL_DIR}.${ColourReset}\n")

# Add folder where are supportive functions
set(CMAKE_UTILS_PATH ${CMAKE_SOURCE_DIR}/CMakeStuff)
set(CMAKE_TOOLCHAINS_PATH ${CMAKE_UTILS_PATH}/toolchains)
set(CMAKE_MODULE_PATH ${CMAKE_UTILS_PATH}/modules)
#message("CMAKE_MODULE_PATH:" ${CMAKE_MODULE_PATH})


# This include must come before all the others
# It must include the config-generated config.h file
# before the others.
#include_directories(${CMAKE_BINARY_DIR})

# And now the source directory contains the .h|.hpp files for its .cpp files.
#include_directories(${CMAKE_SOURCE_DIR})

#set(CMAKE_INCLUDE_CURRENT_DIR ON)

# Include the system's uname that fills in SYSTEM_UNAME_S.
# Sets WIN64 if SYSTEM_UNAME_S is "^.*MING64.*"
include(${CMAKE_UTILS_PATH}/systemUname.cmake)

# Include the various colors we want to use in the output
include(${CMAKE_UTILS_PATH}/outputColors.cmake)

set(CMAKE_COLOR_MAKEFILE ON)
set(CMAKE_VERBOSE_MAKEFILE ON)

message("\n${BoldRed}Configuring build for project ${PROJECT}${ColourReset}\n")

# This export will allow using the flags to be used by
# youcompleteme (vim plugin).
set(CMAKE_EXPORT_COMPILE_COMMANDS 1)

if(EXISTS "${CMAKE_CURRENT_BINARY_DIR}/compile_commands.json" )
	execute_process( COMMAND cmake -E copy_if_different
		${CMAKE_CURRENT_BINARY_DIR}/compile_commands.json
		${CMAKE_CURRENT_SOURCE_DIR}/compile_commands.json
		)
endif()


# We want C++17
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
message(STATUS "${BoldGreen}CMAKE_CXX_COMPILER: ${CMAKE_CXX_COMPILER}${ColourReset}")

#############################################################
# We do not want warnings for unknown pragmas:
message(STATUS "Setting definition -Wno-unknown-pragmas.${ColourReset}")
add_definitions(-Wno-unknown-pragmas)
# Enable warnings and possibly treat them as errors
message(STATUS "${BoldGreen}Setting definition -Wall.${ColourReset}")
add_definitions(-Wall)
message(STATUS "${BoldGreen}Setting definition -Wextra.${ColourReset}")
add_definitions(-Wextra)

if(WARN_AS_ERROR)
	message(STATUS "${BoldYellow}Setting definition -Werror.${ColourReset}")
	add_definitions(-Werror)
endif()

message(STATUS "${BoldRed}CMAKE_SOURCE_DIR: ${CMAKE_SOURCE_DIR}${ColourReset}")


#############################################################
#############################################################
# Platform-specific CMake configuration
if(MXE)
	# Run the following cmake command line:
	# x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 ../development
	include(${CMAKE_TOOLCHAINS_PATH}/mxe-toolchain.cmake)
	#include(${CMAKE_TOOLCHAINS_PATH}/mxe-toolchain-olivier.cmake)

	# Set the name to the systemUname variable because in this situation that name
	# is not found, it it passed as a flag in the command line.
	set(systemUname "mxe")

elseif(UNIX AND NOT APPLE)
	# Run the following cmake command line:
	# cmake -DBUILD_USER_MANUAL=1 -DCMAKE_BUILD_TYPE=Debug

	include(${CMAKE_TOOLCHAINS_PATH}/unix-toolchain.cmake)

elseif(WIN64)
	# Run the following cmake command line:
	# cmake -DBUILD_USER_MANUAL=0 -DCMAKE_BUILD_TYPE=Release ../development

	include(${CMAKE_TOOLCHAINS_PATH}/win10-mingw64-toolchain.cmake)

elseif(APPLE)
	# Run the following cmake command line:
	# cmake -DBUILD_USER_MANUAL=0 -DCMAKE_BUILD_TYPE=Release ../development

	include(${CMAKE_TOOLCHAINS_PATH}/apple-macport-toolchain.cmake)
endif()

message("")
message(STATUS "${BoldGreen}Starting configuration of ${PROJECT}${ColourReset}")
message("")
message(STATUS "${BoldYellow}The build toolchain is: ${SystemUname}${ColourReset}")
message("")


#############################################################
#############################################################
# Essential software configuration
message(STATUS "CMAKE_CURRENT_BINARY_DIR: " ${CMAKE_CURRENT_BINARY_DIR})

# Gee, do not set that to default!
# set(MAKE_TEST 1)

if(NOT CMAKE_BUILD_TYPE)
	set(CMAKE_BUILD_TYPE Release CACHE STRING
		"Type of build, options are: None, Debug, Release, RelWithDebInfo, MinSizeRel."
		FORCE)
endif(NOT CMAKE_BUILD_TYPE)

if(CMAKE_BUILD_TYPE MATCHES "Release")
	message(STATUS "Compiling in release mode.")
	add_definitions("-DQT_NO_DEBUG_OUTPUT")
endif()

if(CMAKE_BUILD_TYPE MATCHES "Debug")
	message(STATUS "Compiling in debug mode with MAKE_TEST: ${MAKE_TEST}.")
	message(STATUS "Add definition -ggdb3 to format debug output for GDB.")
	add_definitions(-ggdb3)
endif()

if(CMAKE_BUILD_TYPE MATCHES "RelWithDebInfo")
	message(STATUS "Compiling in release with debug info mode.")
endif( CMAKE_BUILD_TYPE MATCHES "RelWithDebInfo" )

message(STATUS "${BoldYellow}CMAKE_BUILD_TYPE: ${CMAKE_BUILD_TYPE}.${ColourReset}")


# BOOST
if(NOT Boost_FOUND)
	message(STATUS "Boost not yet found. Searching for it.")
	find_package(Boost COMPONENTS iostreams thread filesystem chrono REQUIRED ) 
endif()
message(STATUS "Boost_LIBRARIES found ${Boost_LIB_VERSION}")
message(STATUS "Boost_INCLUDES: ${Boost_INCLUDE_DIRS}")
message(STATUS "Boost_LIB_DIRS: ${Boost_LIBRARY_DIRS}")


if(QCustomPlot_FOUND)
else (QCustomPlot_FOUND)
	set(QCustomPlot_DIR ${CMAKE_MODULE_PATH})
	find_package(QCustomPlot REQUIRED)
endif(QCustomPlot_FOUND)


if(Alglib_FOUND)
else (Alglib_FOUND)
	set(Alglib_DIR ${CMAKE_MODULE_PATH})
	find_package(Alglib REQUIRED)
endif(Alglib_FOUND)


if(Zstd_FOUND)
else (Zstd_FOUND)
	set(Zstd_DIR ${CMAKE_MODULE_PATH})
	find_package(Zstd REQUIRED)
endif(Zstd_FOUND)


if(SQLite3_FOUND)
else(SQLite3_FOUND)
	set(SQLite3_DIR ${CMAKE_MODULE_PATH})
	find_package(SQLite3 REQUIRED)
endif(SQLite3_FOUND)


if(NOT USEPAPPSOTREE)
	SET (USEPAPPSOTREE 0)
Endif(NOT USEPAPPSOTREE)

if(NOT USEQT4)
	if(NOT USEQT5)
		SET (USEQT5 1)
	endif(NOT USEQT5)
endif(NOT USEQT4)

if(USEQT5)
	if(NOT PAPPSOMSPPWIDGET)
		SET (PAPPSOMSPPWIDGET 1)
	endif(NOT PAPPSOMSPPWIDGET)
endif(USEQT5)

# Set the CMAKE_PREFIX_PATH for the find_library fonction when using non
# standard install location
if(CMAKE_INSTALL_PREFIX)
	set(CMAKE_PREFIX_PATH "${CMAKE_INSTALL_PREFIX}" ${CMAKE_PREFIX_PATH})
endif(CMAKE_INSTALL_PREFIX)

# Subdirectories
message(STATUS "Adding subdirectory src for PROJECT: ${PROJECT}")
add_subdirectory(doc)

message(STATUS "Adding subdirectory doc for PROJECT: ${PROJECT}")
add_subdirectory(src)

# Doxygen
if(UNIX AND NOT APPLE)
	find_package(Doxygen)

	if(DOXYGEN_FOUND)

		add_custom_target (apidoc cp Doxyfile ${CMAKE_BINARY_DIR}/Doxyfile-build
			COMMAND echo "OUTPUT_DIRECTORY=${CMAKE_BINARY_DIR}" >> ${CMAKE_BINARY_DIR}/Doxyfile-build
			COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_BINARY_DIR}/Doxyfile-build
			WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})

		add_custom_target(apidoc-stable cp Doxyfile ${CMAKE_BINARY_DIR}/Doxyfile-stable
			COMMAND echo "OUTPUT_DIRECTORY=${CMAKE_BINARY_DIR}" >> ${CMAKE_BINARY_DIR}/Doxyfile-stable
			COMMAND echo "HTML_HEADER=header.html" >> ${CMAKE_BINARY_DIR}/Doxyfile-stable
			COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_BINARY_DIR}/Doxyfile-stable
			WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})

	endif(DOXYGEN_FOUND)

endif(UNIX AND NOT APPLE)

# Packager
set(CPACK_PACKAGE_NAME "libpappsomspp")
set(CPACK_PACKAGE_VENDOR "PAPPSO Development Team")
set(CPACK_PACKAGE_VERSION "${PAPPSOMSPP_VERSION}")
set(CPACK_PACKAGE_VERSION_MAJOR "${PAPPSOMSPP_VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${PAPPSOMSPP_VERSION_MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${PAPPSOMSPP_VERSION_PATCH}")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "The PAPPSO C++ library for mass spectrometry")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/COPYING")
set(CPACK_RESOURCE_FILE_AUTHORS "${CMAKE_SOURCE_DIR}/AUTHORS")
set(CPACK_RESOURCE_FILE_INSTALL "${CMAKE_SOURCE_DIR}/INSTALL")
set(CPACK_SOURCE_GENERATOR "TGZ")
set(CPACK_SOURCE_IGNORE_FILES
	"CMakeFiles"
	"Makefile"
	"_CPack_Packages"
	"CMakeCache.txt"
	".*\\\\.git"
	".*\\\\.gz"
	".*\\\\.deb"
	".*\\\\.rpm"
	".*\\\\.dmg"
	".*\\\\..*\\\\.swp"
	"src/\\\\..*"
	"src/libbpp*"
	"debian/tmp"
	"debian/libbpp.*/"
	"debian/libbpp.*\\\\.so.*"
	"debian/libbpp.*\\\\.a"
	"debian/libbpp.*\\\\.substvars"
	"debian/libbpp.*\\\\.debhelper"
	"debian/debhelper\\\\.log"
	"html"
	"Core.tag"
	"Testing"
	"build/"
	"wbuild/"
	"buildwin64/"
	"build-stamp"
	"install_manifest.txt"
	"DartConfiguration.tcl"
	${CPACK_SOURCE_IGNORE_FILES}
	)



#dch -Dstretch "message"
#cmake ..
#make deb
# scp libpappsomspp* proteus.moulon.inra.fr:/var/www/apt/incoming
# reprepro -Vb /var/www/apt processincoming default
# apt-get remove libpappsomspp*
#
#  debuild -S -sa
# dput -f olivier-langella *changes


#dpkg-deb -x libpappsomspp-qt4_0.0.4-1_amd64.deb libpappsomspp-qt4_0.0.4-1
#dpkg-gensymbols -v0 -plibpappsomspp-qt4 -Plibpappsomspp-qt4_0.0.4-1 -Osymbols
#cp symbols ../debian/libpappsomspp-qt4.symbols


#dpkg-deb -x libpappsomspp-qt5_0.0.4-1_amd64.deb libpappsomspp-qt5_0.0.4-1
#dpkg-gensymbols -v0 -plibpappsomspp-qt5 -Plibpappsomspp-qt5_0.0.4-1 -Osymbols
#cp symbols ../debian/libpappsomspp-qt5.symbols


if(MACOS)
	SET(CPACK_GENERATOR "Bundle")
ENDIF()

SET(CPACK_SOURCE_PACKAGE_FILE_NAME "${CMAKE_PROJECT_NAME}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
SET(CPACK_DEBSOURCE_PACKAGE_FILE_NAME "lib${CMAKE_PROJECT_NAME}_${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}.orig")
INCLUDE(CPack)

if(UNIX)
	#This creates deb packages:



	add_custom_target(targz
		cpack -G TGZ --config CPackSourceConfig.cmake && tar xvfz pappsomspp-${PAPPSOMSPP_VERSION}.tar.gz
		WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
		COMMENT "Creating .tar.gz" VERBATIM
		)


	add_custom_target(deb
		cd pappsomspp-${PAPPSOMSPP_VERSION} && dpkg-buildpackage
		DEPENDS targz
		WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
		COMMENT "Creating Debian package" VERBATIM
		)

	file(GLOB changesdebs "${CMAKE_BINARY_DIR}/pappsomspp_*.dsc")

	foreach(libfile ${changesdebs})
		SET(changesdeb "${libfile}")
	endforeach(libfile)

	message ("changes debian file : ${changesdeb}")
	#lintian -IEi --pedantic tandem-mass_2013.06.15-1_amd64.changes
	add_custom_target(lintian
		lintian -IEi --pedantic ${changesdeb}
		DEPENDS deb
		WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
		COMMENT "lintian check" VERBATIM
		)



	#dpkg-deb -x libpappsomspp-qt4_0.0.4-1_amd64.deb libpappsomspp-qt4_0.0.4-1
	#dpkg-gensymbols -v0 -plibpappsomspp-qt4 -Plibpappsomspp-qt4_0.0.4-1 -Osymbols
	#cp symbols ../debian/libpappsomspp-qt4.symbols


	#dpkg-deb -x libpappsomspp-qt5_0.0.4-1_amd64.deb libpappsomspp-qt5_0.0.4-1
	#dpkg-gensymbols -v0 -plibpappsomspp-qt5 -Plibpappsomspp-qt5_0.0.4-1 -Osymbols
	#cp symbols ../debian/libpappsomspp-qt5.symbols


	# Install cmake module
	INSTALL(FILES ${CMAKE_MODULE_PATH}/FindPappsomspp.cmake DESTINATION share/cmake/Modules)

	# Install cmake config
	configure_file (${CMAKE_MODULE_PATH}/PappsomsppConfig.cmake ${CMAKE_BINARY_DIR}/PappsomsppConfig.cmake)
	INSTALL(FILES ${CMAKE_BINARY_DIR}/PappsomsppConfig.cmake DESTINATION lib${LIB_SUFFIX}/cmake/pappsomspp)



ENDIF(UNIX)


IF(MAKE_TEST)
	#MESSAGE("MAKE_TEST was set to ${MAKE_TEST}")
	ENABLE_TESTING()
	INCLUDE(CTest)
	if(BUILD_TESTING)
		ADD_SUBDIRECTORY(test)
	ENDIF(BUILD_TESTING)
ENDIF(MAKE_TEST)
