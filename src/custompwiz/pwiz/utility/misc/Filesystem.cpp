//
// $Id$
//
//
// Original author: Matt Chambers <matt.chambers .@. vanderbilt.edu>
//
// Copyright 2008 Spielberg Family Center for Applied Proteomics
//   Cedars Sinai Medical Center, Los Angeles, California  90048
// Copyright 2008 Vanderbilt University - Nashville, TN 37232
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
//

#define PWIZ_SOURCE

#include "Filesystem.hpp"

    #include <sys/types.h>
    #include <sys/stat.h>
    //#include <sys/ioctl.h>
    //#include <glob.h>
    #include <dirent.h>
    #include <unistd.h>
    #include <errno.h>
    #ifndef MAX_PATH
        #define MAX_PATH 255
    #endif

#include <boost/utility/singleton.hpp>
#include "pwiz/utility/misc/random_access_compressed_ifstream.hpp"
#include <boost/filesystem/detail/utf8_codecvt_facet.hpp>
#include <boost/locale/conversion.hpp>
#include <boost/spirit/include/karma.hpp>
//#include <boost/xpressive/xpressive.hpp>
#include <iostream>

using std::string;
using std::vector;
using std::runtime_error;


namespace {

class UTF8_BoostFilesystemPathImbuer : public boost::singleton<UTF8_BoostFilesystemPathImbuer>
{
    public:
    UTF8_BoostFilesystemPathImbuer(boost::restricted)
    {
        std::locale global_loc = std::locale();
        std::locale loc(global_loc, new boost::filesystem::detail::utf8_codecvt_facet);
        bfs::path::imbue(loc);
    }

    void imbue() const {};
};

} // namespace

namespace pwiz {
namespace util {


PWIZ_API_DECL bool running_on_wine()
{
    return false;
}


PWIZ_API_DECL void force_close_handles_to_filepath(const std::string& filepath, bool closeMemoryMappedSections) noexcept(true)
{
}


PWIZ_API_DECL void enable_utf8_path_operations()
{
    UTF8_BoostFilesystemPathImbuer::instance->imbue();
}


namespace
{
    void copy_recursive(const bfs::path& from, const bfs::path& to)
    {
        bfs::copy_directory(from, to);

        for(bfs::directory_entry& entry : bfs::directory_iterator(from))
        {
            bfs::file_status status = entry.status();
            if (status.type() == bfs::directory_file)
                copy_recursive(entry.path(), to / entry.path().filename());
            else if (status.type() == bfs::regular_file)
                bfs::copy_file(entry.path(), to / entry.path().filename());
            else
                throw bfs::filesystem_error("[copy_directory] invalid path type", entry.path(), boost::system::error_code(boost::system::errc::no_such_file_or_directory, boost::system::system_category()));
        }
    }

    void copy_recursive(const bfs::path& from, const bfs::path& to, boost::system::error_code& ec)
    {
        bfs::copy_directory(from, to, ec);
        if (ec.value() != 0)
            return;

        for(bfs::directory_entry& entry : bfs::directory_iterator(from))
        {
            bfs::file_status status = entry.status(ec);
            if (status.type() == bfs::directory_file)
                copy_recursive(entry.path(), to / entry.path().filename(), ec);
            else if (status.type() == bfs::regular_file)
                bfs::copy_file(entry.path(), to / entry.path().filename(), ec);
            else if (ec.value() != 0)
                ec.assign(boost::system::errc::no_such_file_or_directory, boost::system::system_category());
        }
    }
}

PWIZ_API_DECL void copy_directory(const bfs::path& from, const bfs::path& to, bool recursive, boost::system::error_code* ec)
{
    if (!bfs::is_directory(from))
        throw bfs::filesystem_error("[copy_directory] source path is not a directory", from, boost::system::error_code(boost::system::errc::not_a_directory, boost::system::system_category()));

    if (bfs::exists(to))
    {
        if (ec != NULL)
            ec->assign(boost::system::errc::file_exists, boost::system::system_category());
        else
            throw bfs::filesystem_error("[copy_directory] target path exists", to, boost::system::error_code(boost::system::errc::file_exists, boost::system::system_category()));
    }

    if (recursive)
    {
        if (ec != NULL)
            copy_recursive(from, to, *ec);
        else
            copy_recursive(from, to);
    }
    else
    {
        if (ec != NULL)
            bfs::copy_directory(from, to, *ec);
        else
            bfs::copy_directory(from, to);
    }
}


using boost::uintmax_t;

template <typename T>
struct double3_policy : boost::spirit::karma::real_policies<T>
{
    // up to 3 digits total, but no unnecessary precision
    static unsigned int precision(T n)
    {
        double fracPart, intPart;
        fracPart = modf(n, &intPart);
        return fracPart < 0.005 ? 0 : n < 10 ? 2 : n < 100 ? 1 : 0;
    }
    static bool trailing_zeros(T) { return false; }

    template <typename OutputIterator>
    static bool dot(OutputIterator& sink, T n, unsigned int precision)
    {
        if (precision == 0)
            return false;
        return boost::spirit::karma::real_policies<T>::dot(sink, n, precision);
    }
};

PWIZ_API_DECL
string abbreviate_byte_size(uintmax_t byteSize, ByteSizeAbbreviation abbreviationType)
{
    uintmax_t G, M, K;
    string GS, MS, KS;

    switch (abbreviationType)
    {
        default:
        case ByteSizeAbbreviation_IEC:
            G = (M = (K = 1024) << 10) << 10;
            GS = " GiB"; MS = " MiB"; KS = " KiB";
            break;

        case ByteSizeAbbreviation_JEDEC:
            G = (M = (K = 1024) << 10) << 10;
            GS = " GB"; MS = " MB"; KS = " KB";
            break;

        case ByteSizeAbbreviation_SI:
            G = (M = (K = 1000) * 1000) * 1000;
            GS = " GB"; MS = " MB"; KS = " KB";
            break;
    }

    string suffix;
    double byteSizeDbl;

    if( byteSize >= G )
    {
        byteSizeDbl = (double) byteSize / G;
        //byteSizeDbl = round(byteSizeDbl * 100) / 100;
        suffix = GS;
    } else if( byteSize >= M )
    {
        byteSizeDbl = (double) byteSize / M;
        suffix = MS;
    } else if( byteSize >= K )
    {
        byteSizeDbl = (double) byteSize / K;
        suffix = KS;
    } else
    {
        byteSizeDbl = (double) byteSize;
        suffix = " B";
        return lexical_cast<string>(byteSize) + suffix;
    }

    using namespace boost::spirit::karma;
    typedef real_generator<double, double3_policy<double> > double3_type;
    static const double3_type double3 = double3_type();
    char buffer[256];
    char* p = buffer;
    generate(p, double3, byteSizeDbl);
    return std::string(&buffer[0], p) + suffix;
}


PWIZ_API_DECL bool isHTTP(const string& s)
{
    //using namespace boost::xpressive;

    // from URI RFC via http://stackoverflow.com/a/26766402/638445
    //sregex uriRegex = sregex::compile("^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?");
    //return regex_match(s, uriRegex);

    return bal::istarts_with(s, "http://") || bal::istarts_with(s, "https://");
}


PWIZ_API_DECL string read_file_header(const string& filepath, size_t length)
{
    UTF8_BoostFilesystemPathImbuer::instance->imbue();

    string head;
    if (!bfs::is_directory(filepath) && !isHTTP(filepath))
    {
        if (!bfs::exists(filepath))
            throw runtime_error("[read_file_header()] Unable to open file " + filepath + " (file does not exist)");

        random_access_compressed_ifstream is(filepath.c_str());
        if (!is)
            throw runtime_error("[read_file_header()] Unable to open file " + filepath + " (" + strerror(errno) + ")");

        head.resize(length, '\0');
        if (!is.read(&head[0], (std::streamsize)head.size()) && !is.eof())
            throw runtime_error("[read_file_header()] Unable to read file " + filepath + " (" + strerror(errno) + ")");
    }
    return head;
}



} // util
} // pwiz
