// Copyright Filippo Rusconi, GPLv3+

/////////////////////// StdLib includes
#include <limits>


/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes
#include "../../utils.h"
#include "../../exportinmportconfig.h"


#pragma once

namespace pappso
{

struct PMSPP_LIB_DECL ColorMapPlotConfig
{
  DataKind xAxisDataKind = DataKind::unset;
  DataKind yAxisDataKind = DataKind::unset;

  std::size_t keyCellCount = 0;
  std::size_t mzCellCount  = 0;

  double minKeyValue = std::numeric_limits<double>::max();
  double maxKeyValue = std::numeric_limits<double>::min();

  double minMzValue = std::numeric_limits<double>::max();
  double maxMzValue = std::numeric_limits<double>::min();

  ColorMapPlotConfig();

  ColorMapPlotConfig(DataKind x_axis_data_kind,
                     DataKind y_axis_data_kind,
                     std::size_t key_cell_count,
                     std::size_t mz_cell_count,
                     double min_key_value,
                     double max_key_value,
                     double min_mz_value,
                     double max_mz_value);

  QString toString() const;
};


} // namespace pappso
