// Copyright Filippo Rusconi, GPLv3+

/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// Local includes
#include "colormapplotconfig.h"


namespace pappso
{

ColorMapPlotConfig::ColorMapPlotConfig()
{
}

ColorMapPlotConfig::ColorMapPlotConfig(DataKind x_axis_data_kind,
                                       DataKind y_axis_data_kind,
                                       std::size_t key_cell_count,
                                       std::size_t mz_cell_count,
                                       double min_key_value,
                                       double max_key_value,
                                       double min_mz_value,
                                       double max_mz_value)
  : xAxisDataKind(x_axis_data_kind),
    yAxisDataKind(y_axis_data_kind),
    keyCellCount(key_cell_count),
    mzCellCount(mz_cell_count),
    minKeyValue(min_key_value),
    maxKeyValue(max_key_value),
    minMzValue(min_mz_value),
    maxMzValue(max_mz_value)
{
}


QString
ColorMapPlotConfig::toString() const
{
  QString text = QString(
                   "xAxisDataKind: %1 - yAxisDataKind: %2 - "
                   "keyCellCount: %3 - mzCellCount: %4 - minKeyValue: %5 - "
                   "maxKeyValue: %6 - "
                   "minMzValue: %7 - maxMzValue: %8")
                   .arg(static_cast<int>(xAxisDataKind))
                   .arg(static_cast<int>(yAxisDataKind))
                   .arg(keyCellCount)
                   .arg(mzCellCount)
                   .arg(minKeyValue)
                   .arg(maxKeyValue)
                   .arg(minMzValue)
                   .arg(maxMzValue);

  return text;
}

} // namespace pappso
