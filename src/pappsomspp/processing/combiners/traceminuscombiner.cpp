#include <numeric>
#include <limits>
#include <vector>
#include <map>
#include <cmath>
#include <iostream>

#include <QDebug>

#include "traceminuscombiner.h"
#include "../../trace/trace.h"
#include "../../types.h"
#include "../../utils.h"
#include "../../pappsoexception.h"
#include "../../exception/exceptionoutofrange.h"


namespace pappso
{


TraceMinusCombiner::TraceMinusCombiner()
{
}


TraceMinusCombiner::TraceMinusCombiner(int decimal_places)
  : TraceCombiner(decimal_places)
{
}


TraceMinusCombiner::TraceMinusCombiner(const TraceMinusCombiner &other)
  : TraceCombiner(other.m_decimalPlaces)
{
}


TraceMinusCombiner::TraceMinusCombiner(TraceMinusCombinerCstSPtr other)
  : TraceCombiner(other->m_decimalPlaces)
{
}


TraceMinusCombiner::~TraceMinusCombiner()
{
}


MapTrace &
TraceMinusCombiner::combine(MapTrace &map_trace, const Trace &trace) const
{
  // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << " ()"
  //<< "map trace size:" << map_trace.size()
  //<< "trace size:" << trace.size();

  if(!trace.size())
    return map_trace;

  for(auto &current_data_point : trace)
    {

      // If the data point is 0-intensity, then do nothing!
      if(!current_data_point.y)
        continue;

      double x = Utils::roundToDecimals(current_data_point.x, m_decimalPlaces);

      std::map<double, double>::iterator map_iterator;

      std::pair<std::map<pappso_double, pappso_double>::iterator, bool> result;

      result = map_trace.insert(
        std::pair<pappso_double, pappso_double>(x, current_data_point.y));

      if(result.second)
        {
          // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << " ()";
          // The new element was inserted, we have nothing to do.
        }
      else
        {
          // The key already existed! The item was not inserted. We need to
          // update the value.

          result.first->second -= current_data_point.y;
        }
    }

  // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << " ()"
  //<< "Prior to returning map_trace, its size is:" << map_trace.size();

  return map_trace;
}


MapTrace &
TraceMinusCombiner::combine(MapTrace &map_trace_out,
                            const MapTrace &map_trace_in) const
{
  // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << " ()"
  //<< "map trace size:" << map_trace_out.size()
  //<< "trace size:" << trace.size();

  if(!map_trace_in.size())
    return map_trace_out;

  for(auto &map_pair : map_trace_in)
    {

      // If the data point is 0-intensity, then do nothing!
      if(!map_pair.second)
        continue;

      double x = Utils::roundToDecimals(map_pair.first, m_decimalPlaces);

      std::map<double, double>::iterator map_iterator;

      std::pair<std::map<pappso_double, pappso_double>::iterator, bool> result;

      result = map_trace_out.insert(
        std::pair<pappso_double, pappso_double>(x, map_pair.second));

      if(result.second)
        {
          // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << " ()";
          // The new element was inserted, we have nothing to do.
        }
      else
        {
          // The key already existed! The item was not inserted. We need to
          // update the value.

          result.first->second -= map_pair.second;
        }
    }

  // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << " ()"
  //<< "Prior to returning map_trace_out, its size is:" << map_trace_out.size();

  return map_trace_out;
}


} // namespace pappso
