/**
 * \file pappsomspp/filers/filtertandemremovec13.h
 * \date 26/04/2019
 * \author Olivier Langella
 * \brief new implementation of the X!Tandem filter to remove isotopes in an MS2
 * signal
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "filtertandemremovec13.h"

#include "../../massspectrum/massspectrum.h"
#include <algorithm>

using namespace pappso;

FilterTandemRemoveC13::FilterTandemRemoveC13()
{
}


FilterTandemRemoveC13::FilterTandemRemoveC13(const FilterTandemRemoveC13 &other)
{
}

MassSpectrum &
FilterTandemRemoveC13::filter(MassSpectrum &data_points) const
{

  // Reverse-sort the data points (in y decreasing order) so that we get the
  // greatest to the front of the vector and we'll then copy the first n data
  // points to the returned vector. See that return (b < a) ?

  MassSpectrum massSpectrum;
  if(data_points.size() > 0)
    {
      auto it     = data_points.begin();
      auto itNext = data_points.begin() + 1;
      auto itEnd  = data_points.end();

      pappso_double prevMz = it->x;

      while(itNext != itEnd)
        {
          if((itNext->x - prevMz) >= m_arbitrary_range_between_isotopes ||
             itNext->x < m_arbitrary_minimum_mz)
            {
              massSpectrum.push_back(*it);
              it     = itNext;
              prevMz = it->x;
            }
          else if(itNext->y > it->y)
            {
              it = itNext;
            }

          itNext++;
        }
      massSpectrum.push_back(*it);
    }

  data_points = std::move(massSpectrum);
  return data_points;
}


FilterTandemDeisotope::FilterTandemDeisotope()
{
}


FilterTandemDeisotope::FilterTandemDeisotope(const FilterTandemDeisotope &other)
{
}

MassSpectrum &
FilterTandemDeisotope::filter(MassSpectrum &data_points) const
{

  MassSpectrum massSpectrum;

  if(data_points.size() > 0)
    {
      auto it    = data_points.begin() + 1;
      auto itEnd = data_points.end();

      DataPoint dataPoint = data_points.at(0);

      while(it != itEnd)
        {
          if((it->x < m_arbitrary_minimum_mz) ||
             ((it->x - dataPoint.x) >= m_arbitrary_range_between_isotopes))
            {
              massSpectrum.push_back(dataPoint);
              dataPoint = *it;
            }
          else if(it->y > dataPoint.y)
            {
              dataPoint = *it;
            }

          it++;
        }
    }

  data_points = std::move(massSpectrum);
  return data_points;
}
