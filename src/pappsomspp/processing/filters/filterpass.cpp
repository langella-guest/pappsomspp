/**
 * \file pappsomspp/filers/filterpass.cpp
 * \date 26/04/2019
 * \author Olivier Langella
 * \brief collection of filters concerned by Y selection
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "filterpass.h"
#include "../../trace/trace.h"
#include <algorithm>
#include <cmath>
#include "../../massspectrum/massspectrum.h"

using namespace pappso;


FilterLowPass::FilterLowPass(double y_pass) : m_y_pass(y_pass)
{
}
FilterLowPass::FilterLowPass(const FilterLowPass &other)
  : m_y_pass(other.m_y_pass)
{
}
Trace &
FilterLowPass::filter(Trace &data_points) const
{
  Trace new_data_points;
  for(auto &&data_point : data_points)
    {
      if(data_point.y < m_y_pass)
        {
          new_data_points.push_back(data_point);
        }
    }
  data_points = std::move(new_data_points);
  return data_points;
}

FilterHighPass::FilterHighPass(double y_pass) : m_y_pass(y_pass)
{
}
FilterHighPass::FilterHighPass(const FilterHighPass &other)
  : m_y_pass(other.m_y_pass)
{
}
Trace &
FilterHighPass::filter(Trace &data_points) const
{
  Trace new_data_points;
  for(auto &&data_point : data_points)
    {
      if(data_point.y > m_y_pass)
        {
          new_data_points.push_back(data_point);
        }
    }
  data_points = std::move(new_data_points);
  return data_points;
}


FilterHighPassPercentage::FilterHighPassPercentage(double y_pass)
  : m_y_pass_ratio(y_pass)
{
}
FilterHighPassPercentage::FilterHighPassPercentage(
  const FilterHighPassPercentage &other)
  : m_y_pass_ratio(other.m_y_pass_ratio)
{
}
Trace &
FilterHighPassPercentage::filter(Trace &data_points) const
{
  auto it_max = maxYDataPoint(data_points.begin(), data_points.end());
  if(it_max == data_points.end())
    return data_points;
  double pass = (it_max->y * m_y_pass_ratio);
  Trace new_data_points;
  for(auto &&data_point : data_points)
    {
      if(data_point.y > pass)
        {
          new_data_points.push_back(data_point);
        }
    }
  data_points = std::move(new_data_points);
  return data_points;
}


FilterGreatestY::FilterGreatestY(std::size_t number_of_points)
  : m_number_of_points(number_of_points)
{
}


FilterGreatestY::FilterGreatestY(const FilterGreatestY &other)
  : m_number_of_points(other.m_number_of_points)
{
}

Trace &
FilterGreatestY::filter(Trace &data_points) const
{

  // Reverse-sort the data points (in y decreasing order) so that we get the
  // greatest to the front of the vector and we'll then copy the first n data
  // points to the returned vector. See that return (b < a) ?
  if(m_number_of_points >= data_points.size())
    return data_points;

  std::sort(data_points.begin(),
            data_points.end(),
            [](const DataPoint &a, const DataPoint &b) { return (b.y < a.y); });

  data_points.erase(data_points.begin() + m_number_of_points,
                    data_points.end());

  // And now sort the Trace conventionally, that is in x increasing order.
  std::sort(data_points.begin(),
            data_points.end(),
            [](const DataPoint &a, const DataPoint &b) { return (a.x < b.x); });


  return data_points;
}

std::size_t
FilterGreatestY::getNumberOfPoints() const
{
  return m_number_of_points;
}


FilterFloorY::FilterFloorY()
{
}
FilterFloorY::FilterFloorY(const FilterFloorY &other)
{
}
Trace &
FilterFloorY::filter(Trace &data_points) const
{
  for(auto &&dataPoint : data_points)
    {
      dataPoint.y = std::floor(dataPoint.y);
    }
  return data_points;
}


FilterRoundY::FilterRoundY()
{
}
FilterRoundY::FilterRoundY(const FilterRoundY &other)
{
}
Trace &
FilterRoundY::filter(Trace &data_points) const
{
  for(auto &&dataPoint : data_points)
    {
      dataPoint.y = std::round(dataPoint.y);
    }
  return data_points;
}


FilterRescaleY::FilterRescaleY(double dynamic) : m_dynamic(dynamic)
{
}
FilterRescaleY::FilterRescaleY(const FilterRescaleY &other)
  : m_dynamic(other.m_dynamic)
{
}
Trace &
FilterRescaleY::filter(Trace &data_points) const
{
  if(m_dynamic == 0)
    return data_points;
  auto it_max = maxYDataPoint(data_points.begin(), data_points.end());
  if(it_max == data_points.end())
    return data_points;
  double maximum = it_max->y;
  for(auto &&dataPoint : data_points)
    {
      dataPoint.y = (dataPoint.y / maximum) * m_dynamic;
    }
  return data_points;
}
double
FilterRescaleY::getDynamicRange() const
{
  return m_dynamic;
}


MassSpectrumFilterGreatestItensities::MassSpectrumFilterGreatestItensities(
  std::size_t number_of_points)
  : m_filterGreatestY(number_of_points)
{
}

MassSpectrumFilterGreatestItensities::MassSpectrumFilterGreatestItensities(
  const MassSpectrumFilterGreatestItensities &other)
  : m_filterGreatestY(other.m_filterGreatestY)
{
}

MassSpectrum &
MassSpectrumFilterGreatestItensities::filter(MassSpectrum &spectrum) const
{
  m_filterGreatestY.filter(spectrum);
  return spectrum;
}


FilterScaleFactorY::FilterScaleFactorY(double dynamic) : m_factor(dynamic)
{
}
FilterScaleFactorY::FilterScaleFactorY(const FilterScaleFactorY &other)
  : m_factor(other.m_factor)
{
}
Trace &
FilterScaleFactorY::filter(Trace &data_points) const
{
  if(m_factor == 1)
    return data_points;
  for(auto &&dataPoint : data_points)
    {
      dataPoint.y = dataPoint.y * m_factor;
    }
  return data_points;
}
double
FilterScaleFactorY::getScaleFactorY() const
{
  return m_factor;
}
