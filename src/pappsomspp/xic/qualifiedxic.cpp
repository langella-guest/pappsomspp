/*
 * *******************************************************************************
 * * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * *
 * * This file is part of MassChroqPRM.
 * *
 * *     MassChroqPRM is free software: you can redistribute it and/or modify
 * *     it under the terms of the GNU General Public License as published by
 * *     the Free Software Foundation, either version 3 of the License, or
 * *     (at your option) any later version.
 * *
 * *     MassChroqPRM is distributed in the hope that it will be useful,
 * *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * *     GNU General Public License for more details.
 * *
 * *     You should have received a copy of the GNU General Public License
 * *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 * *
 * * Contributors:
 * *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 * implementation
 * ******************************************************************************/

#include <QDebug>
#include "qualifiedxic.h"
#include <vector>

namespace pappso
{


QualifiedXic::QualifiedXic(const MsRunId &msrun_id,
                           pappso_double mz,
                           PrecisionPtr precision)
  : m_msRunId(msrun_id),
    m_mz(mz),
    mp_precision(precision),
    msp_xic(Xic().makeXicSPtr())
{
  qDebug() << "QualifiedXic::QualifiedXic begin";
}

QualifiedXic::~QualifiedXic()
{
}


QualifiedXic::QualifiedXic(const QualifiedXic &toCopy)
  : m_msRunId(toCopy.m_msRunId),
    m_mz(toCopy.m_mz),
    mp_precision(toCopy.mp_precision),
    msp_xic(toCopy.msp_xic)
{
  qDebug() << "QualifiedXic::QualifiedXic copy begin";
}

} // namespace pappso
