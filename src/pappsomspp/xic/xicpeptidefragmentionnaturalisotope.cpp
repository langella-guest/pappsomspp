/*
 * *******************************************************************************
 * * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * *
 * * This file is part of MassChroqPRM.
 * *
 * *     MassChroqPRM is free software: you can redistribute it and/or modify
 * *     it under the terms of the GNU General Public License as published by
 * *     the Free Software Foundation, either version 3 of the License, or
 * *     (at your option) any later version.
 * *
 * *     MassChroqPRM is distributed in the hope that it will be useful,
 * *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * *     GNU General Public License for more details.
 * *
 * *     You should have received a copy of the GNU General Public License
 * *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 * *
 * * Contributors:
 * *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 * implementation
 * ******************************************************************************/

#include "xicpeptidefragmentionnaturalisotope.h"

namespace pappso
{
XicPeptideFragmentIonNaturalIsotope::XicPeptideFragmentIonNaturalIsotope(
  const MsRunId &msrun_id,
  const PeptideNaturalIsotopeAverageSp &naturalIsotopeAverageSp,
  const PeptideFragmentIonSp &peptideFragmentIonSp)
  : QualifiedXic(msrun_id,
                 naturalIsotopeAverageSp.get()->getMz(),
                 naturalIsotopeAverageSp.get()->getPrecision()),
    msp_naturalIsotopeAverage(naturalIsotopeAverageSp),
    msp_peptideFragmentIon(peptideFragmentIonSp)
{
  qDebug() << "XicPeptideFragmentIonNaturalIsotope::"
              "XicPeptideFragmentIonNaturalIsotope begin";

  qDebug() << "XicPeptideFragmentIonNaturalIsotope::"
              "XicPeptideFragmentIonNaturalIsotope 1 "
           << msp_naturalIsotopeAverage.get();

  qDebug() << "XicPeptideFragmentIonNaturalIsotope::"
              "XicPeptideFragmentIonNaturalIsotope 2 "
           << msp_peptideFragmentIon.get();
}

XicPeptideFragmentIonNaturalIsotope::~XicPeptideFragmentIonNaturalIsotope()
{
}

XicPeptideFragmentIonNaturalIsotope::XicPeptideFragmentIonNaturalIsotope(
  const XicPeptideFragmentIonNaturalIsotope &other)
  : QualifiedXic(other)
{
  qDebug() << "XicPeptideFragmentIonNaturalIsotope::"
              "XicPeptideFragmentIonNaturalIsotope copy begin";
  msp_naturalIsotopeAverage = other.msp_naturalIsotopeAverage;
  msp_peptideFragmentIon    = other.msp_peptideFragmentIon;
  qDebug() << "XicPeptideFragmentIonNaturalIsotope::"
              "XicPeptideFragmentIonNaturalIsotope copy end";
}
} // namespace pappso
