
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once

#include "qualifiedxic.h"
#include "../peptide/peptideinterface.h"

namespace pappso
{

class XicPeptideInterface : public QualifiedXic
{
  private:
  const unsigned int m_z;
  const PeptideInterfaceSp msp_peptideInterface;

  protected:
  XicPeptideInterface(const MsRunId &msrun_id,
                      const PeptideInterfaceSp &peptideInterfaceSp,
                      PrecisionPtr precision,
                      unsigned int charge);

  public:
  ~XicPeptideInterface();
  virtual unsigned int
  getCharge() const
  {
    return m_z;
  };
};

} // namespace pappso
