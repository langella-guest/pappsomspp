/**
 * \file pappsomspp/peptide/peptidefragment.h
 * \date 10/3/2015
 * \author Olivier Langella
 * \brief peptide fragment model
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "peptidefragment.h"
#include "../pappsoexception.h"

namespace pappso
{


PeptideFragment::PeptideFragment(const PeptideSp &sp_peptide,
                                 PeptideDirection direction,
                                 unsigned int size)
  : msp_peptide(sp_peptide), m_direction(direction), m_size(size)
{
  if(m_direction == PeptideDirection::Nter)
    {
      std::vector<Aa>::const_iterator it(msp_peptide.get()->begin());
      std::vector<Aa>::const_iterator end(msp_peptide.get()->end());
      unsigned int i = 0;
      while(i < m_size)
        {
          m_mass += it->getMass();
          it++;
          i++;
        }
      m_nterCterCleavage =
        AaModification::getInstance("internal:Cter_hydrolytic_cleavage_HO");
    }
  else
    {
      std::vector<Aa>::const_reverse_iterator it(msp_peptide.get()->rbegin());
      std::vector<Aa>::const_reverse_iterator end(msp_peptide.get()->rend());
      unsigned int i = 0;
      while(i < m_size)
        {
          m_mass += it->getMass();
          it++;
          i++;
        }
      m_nterCterCleavage =
        AaModification::getInstance("internal:Nter_hydrolytic_cleavage_H");
    }
  m_mass += m_nterCterCleavage->getMass();
}


PeptideFragment::PeptideFragment(const PeptideFragment &other)
  : msp_peptide(other.msp_peptide),
    m_direction(other.m_direction),
    m_size(other.m_size)
{
  m_mass = other.m_mass;
}


PeptideFragment::PeptideFragment(PeptideFragment &&toCopy) // move constructor
  : msp_peptide(std::move(toCopy.msp_peptide)),
    m_direction(toCopy.m_direction),
    m_size(toCopy.m_size),
    m_mass(toCopy.m_mass)
{
}


PeptideFragment::~PeptideFragment()
{
}

const QString
PeptideFragment::getPeptideIonDirectionName(PeptideDirection direction)
{
  switch(direction)
    {
      case PeptideDirection::Cter:
        return "Cter";
        break;
      case PeptideDirection::Nter:
        return "Nter";
        break;
      default:
        throw PappsoException(QString("direction name not implemented"));
        break;
    }
}

const QString
PeptideFragment::getSequence() const
{
  QString sequence = msp_peptide.get()->getSequence();
  int diffSize     = msp_peptide.get()->size() - m_size;
  if(m_direction == PeptideDirection::Nter)
    {
      sequence = sequence.mid(0, m_size);
    }
  else
    {
      sequence = sequence.mid(diffSize, m_size);
    }
  return sequence;
}


int
PeptideFragment::getNumberOfAtom(AtomIsotopeSurvey atom) const
{
  int number = 0;
  if(m_direction == PeptideDirection::Nter)
    {
      std::vector<Aa>::const_iterator it(msp_peptide.get()->begin());
      std::vector<Aa>::const_iterator end(msp_peptide.get()->end());
      unsigned int i = 0;
      while(i < m_size)
        {
          number += it->getNumberOfAtom(atom);
          it++;
          i++;
        }
    }
  else
    {
      std::vector<Aa>::const_reverse_iterator it(msp_peptide.get()->rbegin());
      std::vector<Aa>::const_reverse_iterator end(msp_peptide.get()->rend());
      unsigned int i = 0;
      while(i < m_size)
        {
          number += it->getNumberOfAtom(atom);
          it++;
          i++;
        }
    }
  number += m_nterCterCleavage->getNumberOfAtom(atom);
  // qDebug() << "Aa::getMass() end " << mass;
  return number;
}

int
PeptideFragment::getNumberOfIsotope(Isotope isotope) const
{
  int number = 0;
  if(m_direction == PeptideDirection::Nter)
    {
      std::vector<Aa>::const_iterator it(msp_peptide.get()->begin());
      std::vector<Aa>::const_iterator end(msp_peptide.get()->end());
      unsigned int i = 0;
      while(i < m_size)
        {
          number += it->getNumberOfIsotope(isotope);
          it++;
          i++;
        }
    }
  else
    {
      std::vector<Aa>::const_reverse_iterator it(msp_peptide.get()->rbegin());
      std::vector<Aa>::const_reverse_iterator end(msp_peptide.get()->rend());
      unsigned int i = 0;
      while(i < m_size)
        {
          number += it->getNumberOfIsotope(isotope);
          it++;
          i++;
        }
    }
  // number += m_nterCterCleavage->getNumberOfIsotope(atom);
  // qDebug() << "Aa::getMass() end " << mass;
  return number;
}


const PeptideSp &
PeptideFragment::getPeptideSp() const
{
  return msp_peptide;
};
unsigned int
PeptideFragment::size() const
{
  return m_size;
}

pappso_double
PeptideFragment::getMass() const
{
  return m_mass;
}

PeptideDirection
PeptideFragment::getPeptideIonDirection() const
{
  return m_direction;
}

bool
PeptideFragment::isPalindrome() const
{
  return msp_peptide.get()->isPalindrome();
}


} // namespace pappso
