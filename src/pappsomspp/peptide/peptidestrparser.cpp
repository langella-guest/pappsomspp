
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include <QStringList>
#include "peptidestrparser.h"
#include "../obo/filterobopsimodtermlabel.h"
#include "../obo/filterobopsimodsink.h"

namespace pappso
{


QRegExp PeptideStrParser::_mod_parser("\\([^)]*\\)");
QRegExp PeptideStrParser::_rx_psimod("MOD:[0-9]+");
QRegExp PeptideStrParser::_rx_modmass("[-+]?[0-9]*\\.?[0-9]*");

void
PeptideStrParser::parseStringToPeptide(const QString &pepstr, Peptide &peptide)
{
  // Peptide
  // peptide2("C(MOD:00397+MOD:01160)C(MOD:00397)AADDKEAC(MOD:00397)FAVEGPK");
  // CCAADDKEACFAVEGPK
  QRegExp mod_parser(_mod_parser);
  QRegExp rx_psimod(_rx_psimod);
  QRegExp rx_modmass(_rx_modmass);
  /*
  <psimod position="1"  accession="MOD:00397"/>
    <psimod position="2"  accession="MOD:00397"/>
    <psimod position="10"  accession="MOD:00397"/>
    <psimod position="1"  accession="MOD:01160"/>
    */
  int pos                  = 0;
  int matched_length_cumul = 0;
  while((pos = mod_parser.indexIn(pepstr, pos)) != -1)
    {
      QStringList mod_list = pepstr.mid(pos + 1, mod_parser.matchedLength() - 2)
                               .split(QRegExp("[+,\\,]"));
      for(QString &mod : mod_list)
        {
          qDebug() << "PeptideStrParser::parseString mod " << mod;
          if(rx_psimod.exactMatch(mod))
            {
              qDebug() << "PeptideStrParser::parseString pos-1 "
                       << (pos - 1 - matched_length_cumul);
              peptide.addAaModification(AaModification::getInstance(mod),
                                        pos - 1 - matched_length_cumul);
            }
          else if(mod.startsWith("internal:Nter_"))
            {
              peptide.setInternalNterModification(
                AaModification::getInstance(mod));
            }
          else if(mod.startsWith("internal:Cter_"))
            {
              peptide.setInternalCterModification(
                AaModification::getInstance(mod));
            }
          else if(rx_modmass.exactMatch(mod))
            {
              // number
              if(!mod.contains("."))
                {
                  // integer
                  mod = "MOD:0000" + mod;
                  while(mod.size() > 9)
                    {
                      mod = mod.replace(4, 1, "");
                    }
                  peptide.addAaModification(AaModification::getInstance(mod),
                                            pos - 1 - matched_length_cumul);
                }
              else
                {
                  peptide.addAaModification(
                    AaModification::getInstanceCustomizedMod(mod.toDouble()),
                    pos - 1 - matched_length_cumul);
                }
            }
          else
            {
              FilterOboPsiModSink term_list;
              FilterOboPsiModTermLabel filter_label(term_list, mod);

              OboPsiMod psimod(filter_label);

              peptide.addAaModification(
                AaModification::getInstance(term_list.getFirst()),
                pos - 1 - matched_length_cumul);
            }
        }
      matched_length_cumul += mod_parser.matchedLength();
      pos += mod_parser.matchedLength();
    }
}

PeptideSp
PeptideStrParser::parseString(const QString &pepstr)
{

  // QMutexLocker locker(&_mutex);
  Peptide peptide(QString(pepstr).replace(_mod_parser, ""));
  PeptideStrParser::parseStringToPeptide(pepstr, peptide);

  return (peptide.makePeptideSp());
}

NoConstPeptideSp
PeptideStrParser::parseNoConstString(const QString &pepstr)
{

  // QMutexLocker locker(&_mutex);
  Peptide peptide(QString(pepstr).replace(_mod_parser, ""));
  PeptideStrParser::parseStringToPeptide(pepstr, peptide);

  return (peptide.makeNoConstPeptideSp());
}
} // namespace pappso
