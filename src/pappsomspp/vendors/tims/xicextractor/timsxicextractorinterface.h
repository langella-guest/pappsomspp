/**
 * \file pappsomspp/vendors/tims/xicextractor/timsxicextractorinterface.h
 * \date 21/09/2019
 * \author Olivier Langella
 * \brief minimum functions to extract XICs from Tims Data
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include "../../../mzrange.h"
#include "../../../xic/xic.h"
#include "../timsdata.h"

namespace pappso
{

struct TimsXicStructure
{
  std::size_t precursorId;
  MzRange mzRange;
  std::size_t mobilityIndexBegin;
  std::size_t mobilityIndexEnd;
  pappso::pappso_double rtTarget;
  XicSPtr xicSptr     = nullptr;
  double tmpIntensity = 0;
};

/**
 * @todo set of minimum functions to build XICs using Tims data
 */
class TimsXicExtractorInterface
{

  public:
  TimsXicExtractorInterface(TimsData *mp_tims_data);
  virtual ~TimsXicExtractorInterface();

  /** @brief set the XIC extraction method
   */
  void setXicExtractMethod(XicExtractMethod method); // sum or max


  protected:
  /** @brief extract XICs for given coordinates
   * XICs are extracted given their coordinates : retention time target,
   * mobility range, mz range
   * @param timsXicList list of TIMS XIC structures (XIC coordinates)
   * @param rtRange the range in seconds that will be applied before and after
   * XIC rtTarget to extract signal
   */
  virtual void extractTimsXicList(std::vector<TimsXicStructure> &timsXicList,
                                  double rtRange) const = 0;

  protected:
  TimsData *mp_timsData;
  XicExtractMethod m_xicExtractMethod = XicExtractMethod::max;
};

} // namespace pappso
