/**
 * \file pappsomspp/grouping/grpmappeptidegroup.cpp
 * \date 15/12/2017
 * \author Olivier Langella
 * \brief keep trace of peptide to group assignment
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "grpmappeptidetogroup.h"

namespace pappso
{

GrpMapPeptideToGroup::GrpMapPeptideToGroup()
{
}

GrpMapPeptideToGroup::GrpMapPeptideToGroup(const GrpMapPeptideToGroup &other)
  : m_mapPeptideToGroup(other.m_mapPeptideToGroup)
{
}

GrpMapPeptideToGroup::~GrpMapPeptideToGroup()
{
}

void
GrpMapPeptideToGroup::getGroupList(
  const GrpPeptideSet &peptide_set_in,
  std::list<GrpGroupSp> &impacted_group_list) const
{
  auto it_peptide_end = peptide_set_in.m_peptidePtrList.end();
  std::map<GrpPeptide *, GrpGroupSp>::const_iterator it_map_end =
    m_mapPeptideToGroup.end();

  for(auto it_peptide = peptide_set_in.m_peptidePtrList.begin();
      it_peptide != it_peptide_end;
      it_peptide++)
    {
      std::map<GrpPeptide *, GrpGroupSp>::const_iterator it_map =
        m_mapPeptideToGroup.find(*it_peptide);
      if(it_map != it_map_end)
        {
          impacted_group_list.push_back(it_map->second);
        }
    }
  impacted_group_list.sort();
  impacted_group_list.unique();
}

void
GrpMapPeptideToGroup::set(const GrpPeptideSet &peptide_set_in,
                          GrpGroupSp grp_group)
{

  auto it_peptide_end = peptide_set_in.m_peptidePtrList.end();

  for(auto it_peptide = peptide_set_in.m_peptidePtrList.begin();
      it_peptide != it_peptide_end;
      it_peptide++)
    {
      std::pair<std::map<GrpPeptide *, GrpGroupSp>::iterator, bool> ret =
        m_mapPeptideToGroup.insert(
          std::pair<GrpPeptide *, GrpGroupSp>(*it_peptide, grp_group));
      if(ret.second == false)
        { //=> key already exists : replace by grp_group
          ret.first->second = grp_group;
        }
    }
}
void
GrpMapPeptideToGroup::clear(std::list<GrpGroupSp> &grp_group_list)
{

  std::list<GrpGroupSp> new_list;
  for(auto &pair_map : m_mapPeptideToGroup)
    {
      new_list.push_back(pair_map.second);
    }
  new_list.sort();
  new_list.unique();

  grp_group_list.splice(grp_group_list.end(), new_list);
  m_mapPeptideToGroup.clear();
}
} // namespace pappso
