
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once

#include <list>

#include "grpprotein.h"

namespace pappso
{

class GrpMapPeptideToSubGroupSet;

class GrpPeptideSet
{
  friend class GrpMapPeptideToSubGroupSet;
  friend class GrpMapPeptideToGroup;

  private:
  std::list<GrpPeptide *> m_peptidePtrList;

  bool privContainsAll(const GrpPeptideSet &peptideSetIn) const;

  public:
  GrpPeptideSet();
  GrpPeptideSet(const GrpPeptideSet &other);
  GrpPeptideSet(const GrpProtein *p_protein);
  ~GrpPeptideSet();
  GrpPeptideSet &operator=(const GrpPeptideSet &other);
  bool operator==(const GrpPeptideSet &other) const;

  unsigned int
  size() const
  {
    return m_peptidePtrList.size();
  };
  bool biggerAndContainsAll(const GrpPeptideSet &peptideSet) const;
  bool contains(const GrpPeptide *p_grp_peptide) const;
  bool containsAll(const GrpPeptideSet &peptideSet) const;
  bool containsAny(const GrpPeptideSet &peptideSet) const;
  void addAll(const GrpPeptideSet &peptideSet);
  const QString printInfos() const;

  void numbering();
  void setGroupNumber(unsigned int i);

  std::vector<const GrpPeptide *> getGrpPeptideList() const;
};
} // namespace pappso
